var intervalCheck;

$(window).on("load",function(e){
  e.preventDefault();
  e.stopImmediatePropagation();
  $('body').on('show.bs.modal', function() {
    $('.modal-body').overlayScrollbars({
  	  className: "os-theme-round-dark",
      overflowBehavior: {
        x: 'hidden'
      },
      resize: "none",
      scrollbars: {
        autoHide: "never"
      }
  	});
    setTimeout(function(){
      $(".os-viewport.os-viewport-native-scrollbars-invisible").scrollTop(0,0);
    },200);
  });
  $('body').on('hidden.bs.modal',function(){
    $('#contenido').overlayScrollbars({
      className: "os-theme-round-dark",
      autoUpdate: true,
      nativeScrollbarsOverlaid : {
        showNativeScrollbars   : false,
        initialize             : true
      },
      overflowBehavior: {
        x: 'hidden',
        y : "scroll"
      },
      resize: "none",
      scrollbars: {
        autoHide: "never",
        touchSupport: true,
      }
    });
  });
});

function menuElegant(){
  $("#contenido").css("height",$(window).height()-100);
}

function alertasToast(texto,tipo){
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": false,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "2000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "slideDown",
    "hideMethod": "slideUp"
  }
  if(tipo === "check"){
    toastr["success"](texto);
  }
  else if(tipo === "info"){
    toastr["info"](texto);
  }
  else if(tipo === "error"){
    toastr["error"](texto);
  }
  else{
    toastr["info"](texto);
  }
}

$('.input-number').on('input', function () {
    this.value = this.value.replace(/[^0-9]/g,'');
});

$(".input-valid").on('input', function(){
  $(this).removeClass("is-invalid");
});

$("#checkAddress").unbind("click").click(async function(){
  if($("#selectBlock").val() === "bsc"){
    await cargaDatosBSC($.trim($("#address").val()));
  }
})

$("#address").keypress(async function(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if(code==13){
      if($("#selectBlock").val() === "bsc"){
        await cargaDatosBSC($.trim($("#address").val()));
      }
    }
});

async function cargaDatosBSC(address){
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading_crpyto.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Capturing data ...</font>");
  setTimeout(function(){
    $("#textoModalSplash").html("<img src='view/img/loading_crpyto.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>analyzing, please wait ...</font>");
  },5000);
  $('#modalAlertasSplash').modal('show');
  $("#proyectoInfo").val("Cargando...");
  $("#tokenInfo").val("Cargando...");
  $("#tipoInfo").val("Cargando...");
  $("#accionesInfo").val("Cargando...");
  $("#creadorInfo").val("Cargando...");
  $("#webInfo").val("Cargando...");
  $("#emailSocial").val("Cargando...");
  $("#facebookSocial").val("Cargando...");
  $("#twitterSocial").val("Cargando...");
  $("#telegramSocial").val("Cargando...");
  $("#whitepaperSocial").val("Cargando...");
  $("#spanCoinmarketcap").html("Cargando...");
  $("#spanCoinmarketcap").css("color","black");
  $("#spanCoinmarketcap").attr("class","");
  $("#spanCoingeko").html("Cargando...");
  $("#spanCoingeko").css("color","black");
  $("#spanCoingeko").attr("class","");
  $("#spanPancakeswap").html("Cargando...");
  $("#spanPancakeswap").css("color","black");
  $("#spanPancakeswap").attr("class","");
  $("#liquidezPancakeSwap").html("Cargando...");
  $("#liquidezPancakeSwap").css("color","black");
  $("#liquidezPancakeSwap").attr("class","");
  $("#valorUsd").html("Cargando...");
  $("#valorUsd").css("color","black");
  $("#valorUsd").attr("class","");

  if(address !== ""){
    var parametros = {
      "address": address,
      "red": 'binance-smart-chain'
    };
    $.ajax({
      url:   'controller/chequeaBSC.php',
      type:  'post',
      data:  parametros,
      success: async function (response) {
        var p = jQuery.parseJSON(response);
        if(p.length !== 0){
          $("#proyectoInfo").val(p.nombre);
          if(p.token.replace(" Token","").replace(" token","") !== p.token2){
            $("#tokenInfo").val(p.token2.replace(" Token","").replace(" token",""));
          }
          else{
            $("#tokenInfo").val(p.token.replace(" Token","").replace(" token",""));
          }
          $("#tipoInfo").val(p.tipo);
          $("#accionesInfo").val(p.acciones);
          $("#creadorInfo").val(p.creador);
          $("#webInfo").val(p.web);

          var porBurn = 0;
          if(typeof p.holders !== 'undefined'){
            $.each(p.holders, (index, value) =>{
              if(value.address.indexOf("0x0000") > -1 || value.address.indexOf("Burn") > -1 || value.address.indexOf("burn") > -1){
                porBurn = parseFloat(value.percentage);
              }
            });
          }

          if(typeof p.social === 'undefined'){
            $("#emailSocial").val("");
            $("#facebookSocial").val("");
            $("#twitterSocial").val("");
            $("#telegramSocial").val("");
            $("#whitepaperSocial").val("");
          }
          else{
            $("#emailSocial").val(p.social.email);
            $("#facebookSocial").val(p.social.facebook);
            $("#twitterSocial").val(p.social.twitter);
            $("#telegramSocial").val(p.social.telegram);
            $("#whitepaperSocial").val(p.social.whitepaper);
          }


          parametros['alias'] = $("#tokenInfo").val();
          parametros['nombre'] = $("#proyectoInfo").val();

          var aprobadas = 0;
          var bloqueadas = 0;
          var liquidez = 0;
          var transferencias = 0;
          var maxTx = 0;
          var total = 0;
          var transferenciasCuentaxCuenta = 0;
          // var swapToken = 0;

          for(var j = 0; j < p.tx1.length; j++){
            if(p.tx1[j].action === 'Approve'){
              aprobadas++;
              total++;
            }
            if(p.tx1[j].action === 'Transfer'){
              transferencias++;
              total++;
            }
            if(p.tx1[j].action === 'Set Liquidity Fe...'){
              liquidez++;
              total++;
            }
            if(p.tx1[j].action === 'Add To Black Lis...' || p.tx1[j].action === 'Black List Addre...'){
              bloqueadas++;
              total++;
            }
            if(p.tx1[j].action === 'Set Max Tx Perce...'){
              maxTx++;
              total++;
            }
            if(p.tx1[j].action === 'Transfer From'){
              transferenciasCuentaxCuenta++;
              total++;
            }
            // if(p.tx1[j].action.indexOf("Swap") > -1){
            //   swapToken++;
            //   total++;
            // }
          }

          var porScam = 0;
          var totScam = 12;

          var cuerpoScan = '<h4 style="margin-bottom: 20pt;">Transacciones en blockchain:</h4>';
          var cuerpoScan = cuerpoScan + '<span>Aprobadas: <b>' + aprobadas + '</b></span><br>';
          var cuerpoScan = cuerpoScan + '<span>Transferencias: <b>' + transferencias + '</b></span><br>';
          var cuerpoScan = cuerpoScan + '<span>Transferencias cuenta a cuenta: <b>' + transferenciasCuentaxCuenta + '</b></span><br>';
          var cuerpoScan = cuerpoScan + '<span>% Tokens/Monedas quemado/as: <b>' + porBurn + ' %</b></span><br>';
          var cuerpoScan = cuerpoScan + '<span>Liquidez: <b>' + liquidez + '</b></span><br>';
          var cuerpoScan = cuerpoScan + '<span>Billeteras bloqueadas: <b>' + bloqueadas + '</b></span><br>';
          var cuerpoScan = cuerpoScan + '<span>% Max. Transacción: <b>' + maxTx + '</b></span><br>';
          var cuerpoScan = cuerpoScan + '<h6 style="margin-top: 20pt;">Paginas verificadas: <b>' + p.paginas + '</b></h6>';
          var cuerpoScan = cuerpoScan + '<h6>Cantidad transacciones verificadas: <b>' + total + '</b></h6>';

          $("#idBodyScan").html(cuerpoScan);

          var cuerpoAlerta = '<h4 style="margin-bottom: 20pt;">Alertas de Fraude:</h4>';

          if(bloqueadas/aprobadas > 0.1){
            cuerpoAlerta = cuerpoAlerta + '<span>% Billeteras bloqueadas vs Operaciones aprobadas de compra: <b>' + ((bloqueadas/aprobadas)*100).toFixed(2) + '% </b></span><br>';
            porScam = porScam + 5;
          }

          if(p.web == null || p.web === ""){
            cuerpoAlerta = cuerpoAlerta + '<span>Sin sitio web declarado en BSC para chequeo de Roadmap</span><br>';
            porScam = porScam + 1;
          }

          if(typeof p.social === 'undefined'){
            cuerpoAlerta = cuerpoAlerta + '<span>Sin whitepaper declarado en BSC</span><br>';
            cuerpoAlerta = cuerpoAlerta + '<span>Sin redes sociales declaradas en BSC</span><br>';
            porScam = porScam + 2;
          }
          else{
            if(typeof p.social.whitepaper === 'undefined'){
              cuerpoAlerta = cuerpoAlerta + '<span>Sin whitepaper declarado en BSC</span><br>';
              porScam = porScam + 1;
            }
          }

          if(porBurn >= 50){
            cuerpoAlerta = cuerpoAlerta + '<span>% Elevado de tokens quemados</span><br>';
            porScam = porScam + 1;
          }

          if($("#proyectoInfo").val() !== ""){
            await $.ajax({
              url:   'controller/chequeaCMC.php',
              type:  'post',
              data:  parametros,
              success: function (response) {
                if(response !== "0"){
                  $("#spanCoinmarketcap").css("color","green");
                  $("#spanCoinmarketcap").attr("class","fas fa-check-circle");
                  $("#spanCoinmarketcap").html("&nbsp;&nbsp;Disponible");
                }
                else{
                  $("#spanCoinmarketcap").css("color","red");
                  $("#spanCoinmarketcap").attr("class","fas fa-times-circle");
                  $("#spanCoinmarketcap").html("&nbsp;&nbsp;No Disponible");
                  cuerpoAlerta = cuerpoAlerta + '<span>No disponible en Coinmarketcap</span><br>';
                  porScam = porScam + 1;
                }
              }
            });

            await $.ajax({
              url:   'controller/chequeaCGK.php',
              type:  'post',
              data:  parametros,
              success: function (response) {
                if(response !== "0"){
                  $("#spanCoingeko").css("color","green");
                  $("#spanCoingeko").attr("class","fas fa-check-circle");
                  $("#spanCoingeko").html("&nbsp;&nbsp;Disponible");
                }
                else{
                  $("#spanCoingeko").css("color","red");
                  $("#spanCoingeko").attr("class","fas fa-times-circle");
                  $("#spanCoingeko").html("&nbsp;&nbsp;No Disponible");
                  cuerpoAlerta = cuerpoAlerta + '<span>No disponible en Coingeko</span><br>';
                  porScam = porScam + 1;
                }
              }
            });

            await $.ajax({
              url:   'controller/chequeaPKS.php',
              type:  'post',
              data:  parametros,
              success: function (response) {
                var p = jQuery.parseJSON(response);
                if(p.length !== "0"){
                  // console.log(p);
                  $("#spanPancakeswap").css("color","green");
                  $("#spanPancakeswap").attr("class","fas fa-check-circle");
                  $("#spanPancakeswap").html("&nbsp;&nbsp;Disponible");

                  if((parseFloat(p.liquidezPKS) <= 0 || p.liquidezPKS == null) || (parseFloat(p.valorUDS) <= 0 || p.valorUDS == null)){
                    cuerpoAlerta = cuerpoAlerta + '<span>Disponible en PancakeSwap:</span><br>';
                  }

                  if(parseFloat(p.liquidezPKS) <= 0 || p.liquidezPKS == null){
                    cuerpoAlerta = cuerpoAlerta + '<span style="margin-left: 20pt;">• Liquidez: <b>' + accounting.formatMoney(parseFloat(p.liquidezPKS), "$ ", 4) + '</b></span><br>';
                    porScam = porScam + 1;

                    $("#liquidezPancakeSwap").css("color","red");
                    $("#liquidezPancakeSwap").attr("class","fas fa-hand-holding-usd");
                    $("#liquidezPancakeSwap").html("&nbsp;&nbsp; " + accounting.formatMoney(parseFloat(p.liquidezPKS), "$ ", 4));
                  }
                  else{
                    $("#liquidezPancakeSwap").css("color","green");
                    $("#liquidezPancakeSwap").attr("class","fas fa-hand-holding-usd");
                    $("#liquidezPancakeSwap").html("&nbsp;&nbsp; " + accounting.formatMoney(parseFloat(p.liquidezPKS), "$ ", 4));
                  }
                }
                else{
                  $("#spanPancakeswap").css("color","red");
                  $("#spanPancakeswap").attr("class","fas fa-times-circle");
                  $("#spanPancakeswap").html("&nbsp;&nbsp;No Disponible");
                  $("#liquidezPancakeSwap").css("color","red");
                  $("#liquidezPancakeSwap").attr("class","fas fa-hand-holding-usd");
                  $("#liquidezPancakeSwap").html("&nbsp;&nbsp;$ 0");
                  $("#valorUsd").css("color","red");
                  $("#valorUsd").attr("class","fas fa-hand-holding-usd");
                  $("#valorUsd").html("&nbsp;&nbsp;$ 0");
                }
              }
            });

            if(cuerpoAlerta == '<h4 style="margin-bottom: 20pt;">Alertas de Fraude:</h4>'){
              cuerpoAlerta = cuerpoAlerta + '<span style="color: green;">Sin alertas de fraude</span><br>';
              $("#idAlertScan").html(cuerpoAlerta);
            }
            else{
              $("#idAlertScan").html(cuerpoAlerta);
            }

            //Generación de grafica
            var tam = ($(window).height() - 120);
            console.log((100*(porScam/totScam)).toFixed(2));
            await graficaScam(parseInt(100*(porScam/totScam)), tam/3);
            $("#idBlockchain").fadeIn();
            $("#bodyScan").fadeIn();
            $('#modalAlertasSplash').modal("hide");
          }
          else{
            alertasToast("Proyecto no encontrado","info");
            $("#proyectoInfo").val("");
            $("#tokenInfo").val("");
            $("#tipoInfo").val("");
            $("#accionesInfo").val("");
            $("#creadorInfo").val("");
            $("#webInfo").val("");
            $("#spanCoinmarketcap").html("");
            $("#spanCoingeko").html("");
            $("#spanPancakeswap").html("");
            $("#liquidezPancakeSwap").html("");
            $("#valorUsd").html("");
            setTimeout(function(){
              $('#modalAlertasSplash').modal("hide");
            },1000);
          }
        }
        else{
          alertasToast("Dirección invalida, pruebe en otra red","info");
          $("#proyectoInfo").val("");
          $("#tokenInfo").val("");
          $("#tipoInfo").val("");
          $("#accionesInfo").val("");
          $("#creadorInfo").val("");
          $("#webInfo").val("");
          $("#spanCoinmarketcap").html("");
          $("#spanCoingeko").html("");
          $("#spanPancakeswap").html("");
          $("#liquidezPancakeSwap").html("");
          $("#valorUsd").html("");
          $("#emailSocial").val("");
          $("#facebookSocial").val("");
          $("#twitterSocial").val("");
          $("#telegramSocial").val("");
          $("#whitepaperSocial").val("");
          setTimeout(function(){
            $('#modalAlertasSplash').modal("hide");
          },1000);
        }
      }
    });
  }
  else{
    alertasToast("Debe ingresar una dirección","info");
    $("#address").addClass("is-invalid");
    $("#proyectoInfo").val("");
    $("#tokenInfo").val("");
    $("#tipoInfo").val("");
    $("#accionesInfo").val("");
    $("#creadorInfo").val("");
    $("#webInfo").val("");
    $("#spanCoinmarketcap").html("");
    $("#spanCoingeko").html("");
    $("#spanPancakeswap").html("");
    $("#liquidezPancakeSwap").html("");
    $("#valorUsd").html("");
    $("#emailSocial").val("");
    $("#facebookSocial").val("");
    $("#twitterSocial").val("");
    $("#telegramSocial").val("");
    $("#whitepaperSocial").val("");
    setTimeout(function(){
      $('#modalAlertasSplash').modal("hide");
    },1000);
  }
}

async function graficaScam(value, largo){
  var gaugeOptions = {
      chart: {
          height: largo,
          type: 'solidgauge'
      },
      title: null,
      pane: {
          center: ['50%', '85%'],
          size: '120%',
          startAngle: -90,
          endAngle: 90,
          background: {
              backgroundColor:
                  Highcharts.defaultOptions.legend.backgroundColor || '#EEE',
              innerRadius: '60%',
              outerRadius: '100%',
              shape: 'arc'
          }
      },
      exporting: {
          enabled: false
      },
      tooltip: {
          enabled: false
      },
      yAxis: {
          stops: [
            [0.1, '#55BF3B'], // green
            [0.4, '#DDDF0D'], // yellow
            [0.6, '#DF5353'] // red
          ],
          lineWidth: 0,
          tickWidth: 0,
          minorTickInterval: null,
          tickAmount: 2,
          title: {
              y: -80
          },
          labels: {
              y: 16
          }
      },
      plotOptions: {
          solidgauge: {
              dataLabels: {
                  y: 5,
                  borderWidth: 0,
                  useHTML: true
              }
          }
      }
  };

  var chartSpeed = Highcharts.chart('medidorScam', Highcharts.merge(gaugeOptions, {
    yAxis: {
      min: 0,
      max: 100,
      title: {
        text: '<b>Posibilidad de fraude</b>'
      }
    },
    credits: {
      enabled: false
    },
    series: [{
    name: 'Speed',
    data: [value],
    dataLabels: {
      format:
          '<div style="text-align:center">' +
          '<span style="font-size:25px">{y}</span><br/>' +
          '<span style="font-size:12px;opacity:0.4">%</span>' +
          '</div>'
      },
    tooltip: {
      valueSuffix: '%'
      }
    }]

  }));
}
