var app = angular.module("WPApp", ["ngRoute"]);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/home", {
        controller: "homeController",
        controllerAs: "vm",
        templateUrl : "view/home/home.html?idLoad=74"
    })
    .otherwise({redirectTo: '/home'});

    $locationProvider.hashPrefix('');
});

app.controller("homeController", function(){
  setTimeout(async function(){
    $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
    $("#textoModalSplash").html("<img src='view/img/loading_crpyto.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Charging</font>");
    $('#modalAlertasSplash').modal('show');
      $('#contenido').show();
      $('#menu-lateral').show();
      $('#footer').parent().show();
      $('#footer').show();
      $('#header').parent().show();
      $('#header').show();

      if( !/AppMovil|Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $("#selectBlock").select2({
            theme: 'bootstrap4',
            width: 'resolve',
            placeholder: $(this).data('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
            closeOnSelect: !$(this).attr('multiple')
        });
      }
      else{
        $("#header").css("height",120);
        $("#groupSearchAddress").css("margin-top",15);
      }

      var tam = ($(window).height() - 120);
      $("#tokenResultSearch").css("height",tam);

      // $("#idBodyScan").css("height",(tam - 430));


      await menuElegant();
      await graficaScam(0, tam/3);

      setTimeout(function(){
        $('#modalAlertasSplash').modal('hide');
      },4000);
  },200);
});

function menuElegant(){
  $("#contenido").css("height",$(window).height()-100);
}
